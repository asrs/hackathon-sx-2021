# EX02 - TROUBLESHOOTING

Dans cette exercise un container tourne avec un port aléatoire et le nom suivant : ex02

effectuer la commande curl sur le port du container.

Dans le retour de la commande vous devriez avoir une erreur.

Corriger l'erreur afin d'afficher l'ascii-art de fusée.
