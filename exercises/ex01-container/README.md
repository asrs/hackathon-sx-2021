# EX01 - Containerisation

- Utilisé l'archive ex01_image.tar présent a la racine de votre utilisateur
- Importer cette archive avec le tags suivant : localhost/ex01
- Ce container possède un ficher de configuration bash au chemin suivant : /root/.bashrc
  Ce fichier permet d'afficher un message lors du lancement d'un shell.
  Modifier le messages par la chaine suivant : 

-----BEGIN PGP MESSAGE-----

jA0ECQMCVAaf8QpJVDjw0q8B50pcLXjGXN9AfUUuuKwjcS9YPb91qYe4lhZOSMSm
toizViDCCVvlUnxiLLuKCeNjYIHBFqP7bERMmxnpNIDjy6h7smaMdQ1fJiwzrxWC
qX+c8JH7SDzseBA+KVEyPoKX8Ph0rJVHV+gBJtDj77yVtYRp9CT2ChqbdvyWryzx
H0QxQbY/W+nOo/RKI+KTGiMm471yCDYw9+hqAXw9/DNV6q8LgkgIeThQK2o+RMEu
=H9AQ
-----END PGP MESSAGE-----

- crée un commit de se container nommé avec votre login comme suit: localhost/$LOGIN

- Bonus: Dechiffre le message a l'aide du mot de passe suivant : startx
   et stock le a la racine de ton utilisateur avec le nom suivant : decrypted_art.txt
