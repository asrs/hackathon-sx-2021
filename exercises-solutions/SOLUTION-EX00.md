---

vars:
 tree:
    - parent: linux-history
      distro:
        - name: Redhat
          mode: 740
        - name: Debian
          mode: 740
        - name: Fedora
          mode: 740
        - name: Centos
          mode: 740
        - name: Oracle
          mode: 740
        - name: Ubuntu
          mode: 740
        - name: Kali
          mode: 740
        - name: Parrot
          mode: 740
